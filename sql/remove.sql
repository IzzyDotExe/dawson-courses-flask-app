--drop tables
drop table courses_elements CASCADE CONSTRAINTS;
drop table courses CASCADE CONSTRAINTS;
drop table terms CASCADE CONSTRAINTS;
drop table elements CASCADE CONSTRAINTS;
drop table competencies CASCADE CONSTRAINTS;
drop table domains CASCADE CONSTRAINTS;

--Drop Views
drop view view_courses CASCADE CONSTRAINTS;
drop view view_courses_elements CASCADE CONSTRAINTS;
drop view view_courses_terms CASCADE CONSTRAINTS;
drop view view_courses_domains CASCADE CONSTRAINTS;
drop view view_competencies CASCADE CONSTRAINTS;
drop view view_competencies_elements CASCADE CONSTRAINTS;
drop view view_courses_elements_competencies CASCADE CONSTRAINTS;

drop table AUDIT_LOGS CASCADE CONSTRAINTS;

--Drop Package
drop package courses_package;

--Drop Object
drop type course_typ;
drop type term_typ;
drop type domain_typ;
drop type element_array;
drop type element_typ;
drop type competency_typ;