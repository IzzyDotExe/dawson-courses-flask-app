# CoursesApp

Repo: https://gitlab.com/420-group-1-project/project

Deployment: http://10.172.23.254:8000/

## Group Members
1. Israel Aristide 2135617
2. Samir Abo-Assfour 2137408
3. Jack Hannan 2037915

## Dev steps
1. Install python 3.7+
2. Run the ``setup-venv.bat`` script on windows or ``setup-venv.sh`` script on linux and follow the stpes it gives you
3. Connect to the dawson network or VPN using fortinet
4. Set DBUSER and DBPWD as your username and password in your environment variables
5. Run ./sql/setup.sql on the database to initialize
6. Make changes and run using the built in runners for vscode or pycharm or run in the termial using:
> ```flask --app CoursesApp --debug run --host 0.0.0.0```

## Deploy steps (LINUX ONLY)
1. Install python 3.7+
2. Run the  ``setup-venv.sh`` script and follow the stpes it gives you
3. Connect to the dawson network or VPN using fortinet
4. Set DBUSER and DBPWD as your username and password in your environment variables
5. Run ./sql/setup.sql on the database to initialize
6. Run the following command to serve
> ```gunicorn -b 0.0.0.0:8000 'CoursesApp:create_app()'```
