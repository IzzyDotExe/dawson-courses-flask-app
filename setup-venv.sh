#!/bin/bash

echo "Creating venv..."
python3 -m venv .venv

echo "Installing requirements"
./.venv/bin/pip install -r requirements.txt

echo "Venv created and deps are installed! To activate run ./.venv/bin/activate"

