from wtforms.validators import DataRequired, NumberRange
from flask_wtf import FlaskForm
from wtforms.widgets import TextArea
from wtforms import StringField, DecimalField, IntegerField, SelectField


class Course:
    def __init__(self, course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id):
        if not isinstance(course_id, str) or len(course_id) > 10 or not course_id:
            raise TypeError(
                "ERROR! The id can't exceed 10 characters and should not be null (empty)")
        elif not isinstance(course_title, str) or len(course_title) > 50 or not course_title:
            raise TypeError(
                "ERROR! The course title cannot exceed 50 characters and should not be null (empty)")
        elif not isinstance(theory_hours, int) or theory_hours < 0:
            raise TypeError(
                "ERROR! The number of hours for theory class should not be a string and should not be 0 equal or lower")
        elif not isinstance(lab_hours, int) or lab_hours < 0:
            raise TypeError(
                "ERROR! The lab hours should not be a string and should not be lower or equal to 0")
        elif not isinstance(work_hours, int) or work_hours < 0:
            raise TypeError(
                "ERROR! The homework hours should not be string and should not be lower or equal to 0")
        elif not isinstance(description, str) or len(description) > 1000 or not description:
            raise TypeError(
                "ERROR! The description text should not exceed 1000 characters and should not be null (empty)")
        elif not isinstance(domain_id, int) or domain_id < 0:
            raise TypeError(
                "ERROR! The domain id should not be string and should be a number that is not be lower or equal to 0")
        elif not isinstance(term_id, int) or term_id < 0:
            raise TypeError(
                "ERROR! The term id should not be string and should be a number that is not be lower or equal to 0")

        self.course_id = course_id
        self.course_title = course_title
        self.theory_hours = theory_hours
        self.lab_hours = lab_hours
        self.work_hours = work_hours
        self.description = description
        self.domain_id = domain_id
        self.term_id = term_id
        # cannot use get_db() in this class so will need to put it in view for now
        self.domain = None
        self.term = None

    def __repr__(self):
        return f'<Course | course_id: {self.course_id}, course_title: {self.course_title}, theory_hours: {self.theory_hours}, lab_hours: {self.lab_hours}, work_hours: {self.work_hours}, description: {self.description}, domain_id: {self.domain_id}, term_id: {self.term_id}>'

    def __str__(self):

        if self.domain is not None and self.term is not None:
            return f"""
            <ul>
                <li><a href="{{ url_for('courses.get_course', course_id=self.course_id )}}">{self.course_id}</a></li>
                <li>{self.course_title}</li>
                <li>Ponderation: {self.theory_hours} {self.lab_hours} {self.work_hours}</li>
                <li>{self.description}</li>
                <li>{self.domain}</li>
                <li>{self.term}</li>
            </ul>
            """
        else:
            return f"""
            <ul>
                <li>{self.course_id}</li>
                <li>{self.course_title}</li>
                <li>Ponderation: {self.theory_hours} {self.lab_hours} {self.work_hours}</li>
                <li>{self.description}</li>
                <li>Domain ID: {self.domain_id}</li>
                <li>Term ID: {self.term_id}</li>
            </ul>
            """


class CourseForm(FlaskForm):
    course_id = StringField('course id', validators=[DataRequired()])
    course_title = StringField('course title', validators=[DataRequired()])
    theory_hours = IntegerField('c', validators=[DataRequired(), NumberRange(min=0, max=168,
                                                                             message="This value is for theory hours every week")])
    lab_hours = IntegerField('l', validators=[DataRequired(), NumberRange(min=0, max=168,
                                                                          message="This value is for lab hours every week")])
    work_hours = IntegerField('h', validators=[DataRequired(), NumberRange(min=0, max=168,
                                                                           message="This value is for work hours every week")])
    description = StringField('description', widget=TextArea(), validators=[DataRequired()])
    domain = SelectField('domains', choices=None, validators=[DataRequired()])
    term = SelectField('terms', choices=None, validators=[DataRequired()])


class CourseElement(FlaskForm):

    element = SelectField('element', validators=[DataRequired()])
    hours = DecimalField('hours', validators=[DataRequired()])
    pass
