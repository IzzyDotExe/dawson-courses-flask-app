from flask_wtf import FlaskForm
from wtforms import SelectField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class Term:

    def __init__(self, id, year, name):
        if not isinstance(id, int):
            raise TypeError("Term id should be an int")
        if not isinstance(year, int) or year < 0 or year > 6:
            raise TypeError(
                "Term year must be of type int, must be between 1 and 6")
        if not isinstance(name, str) or len(name) > 6:
            raise TypeError("Term name must be a max 6 character string")

        self.year = year
        self.name = name
        self.id = id

    def __repr__(self) -> str:
        return f"<Term | id: {self.id}, year: {self.year}, name: {self.name}>"

    def __str__(self) -> str:
        return f"""
        <h3>Term</h3>
        <ul>
            <li>ID: {self.id}</li>
            <li>Year: {self.year}</li>
            <li>Season: {self.name}</li>
        </ul>
        """


class TermForm(FlaskForm):
    # id will be auto generated
    year = IntegerField('Year', validators=[DataRequired()])
    season = SelectField('Season', choices=[
        "Fall", "Winter", "Summer"], validators=[DataRequired(), NumberRange(min=1, max=6)])
