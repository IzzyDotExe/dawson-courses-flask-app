from wtforms.validators import DataRequired, Length
from wtforms import StringField, SelectField
from wtforms.widgets import TextArea
from flask_wtf import FlaskForm


class Competency:
    def __init__(self, competency, achievement, type, id):
        if not isinstance(id, str) and (id > 4 or id < 4):
            raise TypeError
        if not isinstance(competency, str):
            raise TypeError
        if not isinstance(achievement, str):
            raise TypeError
        if not isinstance(type, str):
            raise TypeError
        self.id = id
        self.competency = competency
        self.achievement = achievement
        self.type = type

    def __repr__(self):
        return f'<Competency | id: {self.id}, competency: {self.competency}, achievement: {self.achievement}, type: {self.type}>'

    def __str__(self):
        return f"""
        <h2>Competency</h2>
            <li>ID: {self.id}</li>
            <li>Competency: {self.competency}</li>
            <li>Achievement: {self.achievement}</li>
            <li>Type: {self.type}</li>
        """

    def __eq__(self, obj):
        if isinstance(obj, Competency):
            return (obj.competency.upper() == self.competency.upper() and
                    obj.achievement.upper() == self.achievement.upper() and
                    obj.type.upper() == self.type.upper())
        return False


class CompetencyForm(FlaskForm):
    competency_name = StringField(
        'competency_name', widget=TextArea(), validators=[DataRequired(), Length(max=250)])
    achievement = StringField(
        'achievement', widget=TextArea(), validators=[DataRequired(), Length(max=250)])
    type = SelectField(
        'type', choices=['Mandatory', 'Optional'], validators=[DataRequired()])
    id = StringField('id', validators=[DataRequired(), Length(min=4,max=4)])
