import os
import datetime as dt

from flask import jsonify, flash
import oracledb
from CoursesApp.dawson_db_objects.course import Course
from CoursesApp.dawson_db_objects.domain import Domain
from CoursesApp.dawson_db_objects.term import Term

from CoursesApp.dawson_db_objects.element import Element
from werkzeug.security import generate_password_hash

from .dawson_db_objects.competency import Competency

from CoursesApp.user_manage_objects.userprofile import Profile
from .user_manage_objects.user import User, Role


class Database:
    def __init__(self, autocommit=True):
        self.__connection = self.__connect()
        self.__connection.autocommit = autocommit

    def run_file(self, file_path):
        statement_parts = []
        with self.__get_cursor() as cursor:
            with open(file_path, 'r') as f:
                for line in f:
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join(
                            statement_parts).strip().rstrip(';')
                        if statement:
                            cursor.execute(statement)

                        statement_parts = []

    def log_login(self, username):
        
        with self.__get_cursor() as cur:
            stmt = "merge into login_log l using dual on (username = :uname) " \
                    "when not matched then insert (username, logindate) values (:uname, SYSDATE) " \
                    "when matched then update set logindate = SYSDATE"
            cur.execute(stmt, {"uname": username})

    def get_recent_users(self):
        
        with self.__get_cursor() as cur:
            stmt = "SELECT username, logindate from login_log ORDER BY logindate DESC OFFSET 0 ROWS FETCH NEXT 4 ROWS ONLY"
            cur.execute(stmt)
            fetch = cur.fetchall()
            
        names = {}
        for row in fetch:
            names[row[0]] = row[1]
            
        users = self.get_users()
        users = [(x, names[x.username]) for x in users if x.username in names.keys()]
        
        return users

    # Profile Feature------------------------------------------------------

    def create_profile(self, profile: Profile):
        with self.__get_cursor() as cur:

            stmt = "SELECT count(id) FROM user_profiles"
            cur.execute(stmt)

            fetch = cur.fetchone()[0] + 1

            stmt = "INSERT INTO user_profiles(id, name, email, bio, avatar_url)" \
                   "VALUES (:id, :name, :email, :bio, :avatar_url)"
            cur.execute(stmt, [fetch, profile.name,
                        profile.email, profile.bio, profile.avatar_url])

            return fetch
    
    def create_user(self, user: User):
        with self.__get_cursor() as cur:
            uid = None
            if not user.profile.id:
                uid = self.create_profile(user.profile)
                user.profile.id = uid
            stmt = "INSERT INTO course_users(username, password, user_role, profile) " \
                   "VALUES (:uname, :pwd, :role, :profile)"
            cur.execute(
                stmt, [user.username, user.password, user.role.id, uid])

        return user

    def delete_user(self, user: User):

        with self.__get_cursor() as cur:
            stmt = "DELETE FROM course_users WHERE username = :uname"
            cur.execute(stmt, [user.username])

    # Term Feature------------------------------------------------------

    def insert_term(self, term):
        if not isinstance(term, Term):
            raise TypeError("ERROR! This is not a Term!")
        with self.__get_cursor() as cursor:
            cursor.execute(
                "INSERT into terms (term_year, term_name) values (:term_year, :term_name)",
                term_year=term.year, term_name=term.name)

    def get_terms(self):
        with self.__get_cursor() as cur:
            stmt = "SELECT TERM_ID, TERM_YEAR, TERM_NAME FROM TERMS"
            cur.execute(stmt)
            fetch = cur.fetchall()
            terms = []

            if fetch:
                for result in fetch:
                    terms.append(Term(result[0], result[1], result[2]))

            return terms

    def get_term(self, id):
        term_id = int(id)
        terms = self.get_terms()
        for term in terms:
            if term.id == term_id:
                return term
        return None


    def remove_term(self, id):
        stmt = "DELETE FROM TERMS WHERE TERM_ID = :id"
        with self.__get_cursor() as cur:
            cur.execute(stmt, id=id)

    def modify_term(self, term):
        if not isinstance(term, Term):
            raise TypeError("ERROR! This is not a Term!")
        with self.__get_cursor() as cursor:
            cursor.execute('UPDATE terms SET term_year=:updated_year, term_name=:updated_name WHERE term_id=:old_id',
                           updated_year=term.year, updated_name=term.name, old_id=term.id)
            
    # -----------------------------------------------------

    def add_user(self, user: User):
        with self.__get_cursor() as cur:
            stmt = "DELETE FROM course_users WHERE username = :uname"
            cur.execute(stmt, [user.username])

    def update_user(self, user: User):
        with self.__get_cursor() as cur:
            stmt = "UPDATE course_users SET " \
                   "password = :pwd, " \
                   "user_role = :role, " \
                   "profile = :profile " \
                   "WHERE username = :uname"
            cur.execute(stmt, [user.password, user.role.id,
                        user.profile.id, user.username])

    def get_user(self, username) -> User:
        users = self.get_users()
        user = [x for x in users if x.username == username]

        if user:
            return user[0]

        return None

    def get_users(self) -> list[User]:
        stmt = "SELECT username, password, user_role, profile FROM course_users"
        with self.__get_cursor() as cur:

            cur.execute(stmt)
            fetch_result = cur.fetchall()
            users = []

            if fetch_result:
                for result in fetch_result:
                    users.append(User(result[0], result[1], self.get_role(
                        result[2]), self.get_profile(result[3])))
                    
            return users

    # Element Feature----------------------------------------------------

    def get_element(self, element_id):
        if not isinstance(element_id, int):
            raise TypeError("The element id is not an integer")
        elements = self.get_all_elements()
        element = [x for x in elements if x.id == element_id]

        if element:
            return element[0]

        return None

    def get_all_elements(self):
        elements = []
        try:
            stmt = "SELECT element_id, element_order, element, element_criteria, competency_id from elements"
            with self.__get_cursor() as cursor:
                result = cursor.execute(stmt)
                for row in result:
                    elements.append(
                        Element(row[0], row[1], row[2], row[3], row[4]))
                return elements
        except oracledb.Error as e:
            flash(f"ERROR! There was an error in the Server: {str(e)}")
        
    def get_elements(self, competency_id):
        elements = []
        stmt = "SELECT element_id, element_order, element, element_criteria, competency_id from elements where competency_id=:id"
        with self.__get_cursor() as cursor:
            result = cursor.execute(stmt, {"id": competency_id})
            for row in result:
                elements.append(
                    Element(row[0], row[1], row[2], row[3], row[4]))
            return elements
    
    def get_next_id(self):
        with self.__get_cursor() as cursor:
            new_id = cursor.callfunc("courses_package.add_automated_id", int, ())
        return new_id

    def insert_element(self, competency_id, element):
        if not isinstance(element, Element):
            raise TypeError("ERROR! This is not an Element!")
        with self.__get_cursor() as cursor:
            cursor.execute(
                "INSERT into elements (element_order, element, element_criteria, competency_id) values (:element_order, :element_title, :element_criteria, :competency_id)",
                element_order=element.order, element_title=element.element, element_criteria=element.criteria, competency_id=competency_id)

    def update_element(self, element):
        if not isinstance(element, Element):
            raise TypeError("ERROR! This is not an Element!")

        with self.__get_cursor() as cursor:
            cursor.callproc('courses_package.update_element', [
                            element.id, element.order, element.element, element.criteria, element.competency_id])

    def delete_element(self, element_id):
        with self.__get_cursor() as cursor:
            cursor.execute(
                "DELETE from elements where element_id=:id", id=element_id)

    # Competency Feature----------------------------------------------------
    def add_compentency(self, competency):
        if not isinstance(competency, Competency):
            raise TypeError("ERROR! This is not a Competency!")
        with self.__get_cursor() as cursor:
            cursor.execute("INSERT INTO competencies VALUES (:competency_id, :competency, :competency_achievement, :competency_type)",
                            competency_id=competency.id, competency=competency.competency, competency_achievement=competency.achievement, competency_type=competency.type)

    def get_competencies(self):
        competencies_list = []
        with self.__get_cursor() as cursor:
            result = cursor.execute(
                "select competency_id, competency, competency_achievement, competency_type from competencies")
            result = list(result)
            for row in result:
                new_comp = Competency(row[1], row[2], row[3], row[0])
                competencies_list.append(new_comp)

            return competencies_list

    def get_competency(self, id):
        with self.__get_cursor() as cursor:
            result = cursor.execute(
                "select competency_id, competency, competency_achievement, competency_type from competencies where competency_id = :id", id=id)
            result = result.fetchone()
            if result:
                competency = Competency(
                    result[1], result[2], result[3], result[0])
                return competency
        return None

    def delete_competency(self, id):

        with self.__get_cursor() as cursor:
            cursor.callproc('courses_package.delete_competency', [id])

    def modify_competency(self, competency):
        if not isinstance(competency, Competency):
            raise TypeError("ERROR! This is not a Competency!")

        with self.__get_cursor() as cursor:
            cursor.callproc('courses_package.update_competency', [
                            competency.id, competency.competency, competency.achievement, competency.type])

    def reset_password(self, username, passwd):
        user = self.get_user(username)
        user.password = generate_password_hash(passwd)
        self.update_user(user)

    def get_role(self, id):
        roles = self.get_roles()
        role = [x for x in roles if x.id == id]
        if role:
            return role[0]
        return None

    def get_roles(self) -> list[Role]:
        stmt = "SELECT role_id, role, permission_level FROM user_roles"
        with self.__get_cursor() as cur:
            cur.execute(stmt)
            fetch_result = cur.fetchall()
            roles = []

            if fetch_result:
                for result in fetch_result:
                    roles.append(Role(result[1], result[2], result[0]))

            return roles

    def get_profile(self, id):
        profiles = self.get_profiles()
        profile = [x for x in profiles if x.id == id]
        if profile:
            return profile[0]
        return None

    def get_profiles(self):
        stmt = "SELECT id, name, email, bio, avatar_url FROM user_profiles"
        with self.__get_cursor() as cur:
            cur.execute(stmt)
            fetch_result = cur.fetchall()
            profiles = []
            if fetch_result:
                for result in fetch_result:
                    profiles.append(
                        Profile(result[1], result[2], result[3], result[4], result[0]))

            return profiles

    def update_profile(self, profile):
        stmt = "UPDATE user_profiles SET " \
               "name = :name, " \
               "email = :email, " \
               "bio = :bio, " \
               "avatar_url = :avatar " \
               "WHERE id = :id"

        with self.__get_cursor() as cur:
            cur.execute(stmt, [profile.name, profile.email,
                        profile.bio, profile.avatar_url, profile.id])

    # Course Feature----------------------------------------------------
    def get_course(self, course_id):
        course_id = str(course_id)
        courses = self.get_courses()
        for course in courses:
            if course.course_id == course_id:
                return course
        return None

    def get_courses(self):
        courses = []
        stmt = "SELECT course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses"
        with self.__get_cursor() as cursor:
            result = cursor.execute(stmt)
            for row in result:
                courses.append(
                    Course(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
            for course in courses:
                course.domain = self.get_domain(course.domain_id)
                course.term = self.get_term(course.term_id)
            return courses

    def insert_course(self, course):
        if not isinstance(course, Course):
            raise TypeError("ERROR! This is not a Course!")
        with self.__get_cursor() as cursor:
            cursor.execute(
                "INSERT into courses (course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id) values (:course_id, :course_title, :theory_hours, :lab_hours, :work_hours, :description, :domain_id, :term_id)",
                course_id=course.course_id, course_title=course.course_title, theory_hours=course.theory_hours, lab_hours=course.lab_hours, work_hours=course.work_hours, description=course.description, domain_id=course.domain_id, term_id=course.term_id)

    def delete_course(self, course_id):
        with self.__get_cursor() as cursor:
            cursor.execute(
                "DELETE from courses where course_id=:id", id=course_id)

    def modify_course(self, course):
        if not isinstance(course, Course):
            raise TypeError("ERROR! This is not a Course!")
        with self.__get_cursor() as cursor:
            cursor.execute('UPDATE courses SET course_title=:updated_title,\
                            theory_hours=:updated_theory, lab_hours=:updated_lab, work_hours=:updated_work,\
                            description=:updated_description, domain_id=:updated_domain, term_id=:updated_term\
                            WHERE course_id=:course_id',
                           updated_title=course.course_title,
                           updated_theory=course.theory_hours, updated_lab=course.lab_hours, updated_work=course.work_hours,
                           updated_description=course.description, updated_domain=course.domain_id, updated_term=course.term_id,
                           course_id=course.course_id)

    # --------------------------------------------------------------------

    # Domain Feature------------------------------------------------------

    def get_domain(self, domain_id):
        domain_id = int(domain_id)
        domains = self.get_domains()
        for domain in domains:
            if domain.id == domain_id:
                return domain
        return None

    def get_domains(self):
        domains = []
        stmt = "SELECT domain_id, domain, domain_description from domains"
        with self.__get_cursor() as cursor:
            result = cursor.execute(stmt)
            for row in result:
                domains.append(
                    Domain(row[0], row[1], row[2]))
            return domains

    def insert_domain(self, domain):
        if not isinstance(domain, Domain):
            raise TypeError("ERROR! This is not a Domain!")
        with self.__get_cursor() as cursor:
            cursor.execute(
                "INSERT into domains (domain, domain_description) values (:domain_name, :domain_description)",
                domain_name=domain.domain, domain_description=domain.description)

    def delete_domain(self, domain_id):
        with self.__get_cursor() as cursor:
            cursor.execute(
                "DELETE from domains where domain_id=:id", id=domain_id)

    def modify_domain(self, domain):
        if not isinstance(domain, Domain):
            raise TypeError("ERROR! This is not a Domain!")
        with self.__get_cursor() as cursor:
            cursor.execute('UPDATE domains SET domain=:updated_domain, domain_description=:updated_description WHERE domain_id=:old_id',
                           updated_domain=domain.domain, updated_description=domain.description, old_id=domain.id)

    # --------------------------------------------------------------------

    def remove_course_element(self, course_id, element_id):

        if course_id not in [x.course_id for x in self.get_courses()]:
            raise ValueError("Course does not exist")

        if element_id not in [x.id for x in self.get_all_elements()]:
            raise ValueError("Element does not exist")

        if element_id not in [x[0].id for x in self.get_course_elements(course_id)]:
            raise ValueError("Course does not contain that element")

        with self.__get_cursor() as cur:
            stmt = "DELETE FROM courses_elements WHERE course_id=:cid AND element_id=:eid"
            cur.execute(stmt, [course_id, element_id])

    def add_course_element(self, course_id, element_id, hours):

        if course_id not in [x.course_id for x in self.get_courses()]:
            raise ValueError("Course does not exist")

        if element_id not in [x.id for x in self.get_all_elements()]:
            raise ValueError("Element does not exist")

        with self.__get_cursor() as cur:
            stmt = "INSERT INTO courses_elements(course_id, element_id, element_hours) VALUES (:cid, :elid, :hours)"
            cur.execute(stmt, [course_id, element_id, hours])

    def get_course_elements(self, course_id):

        if course_id not in [x.course_id for x in self.get_courses()]:
            raise ValueError("Course does not exist")

        with self.__get_cursor() as cur:
            stmt = "SELECT element_id, element_hours from courses_elements WHERE course_id = :cid"
            cur.execute(stmt, [course_id])
            fetch = cur.fetchall()
            results = []
            if fetch:
                for row in fetch:
                    results.append((self.get_element(row[0]), row[1]))
            return results

    def close(self):
        """Closes the connection"""
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

    def __get_cursor(self):
        for i in range(3):
            try:
                return self.__connection.cursor()
            except Exception as e:
                # Might need to reconnect
                self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__connection = self.__connect()

    def __connect(self):
        return oracledb.connect(user=os.environ['DBUSER'], password=os.environ['DBPWD'],
                                host="198.168.52.211", port=1521, service_name="pdbora19c.dawsoncollege.qc.ca")
