
from wtforms import StringField, EmailField, URLField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import TextArea
from flask_wtf import FlaskForm


class Profile:
    
    @staticmethod
    def default():
        return Profile('Jhon Doe', "example@dawsoncollege.qc.ca", "I'm new here, say hi!", "https://th.bing.com/th/id/OIP.kIq5UJ6W1AIeaC3XPiinDAAAAA?pid=ImgDet&rs=1")
    
    def __init__(self, name, email, bio, avatar_url, id=None):
        
        if type(name) != str or len(name) > 100:
            raise TypeError("Name input was invalid")
        
        if type(email) != str or len(email) > 100:
            raise TypeError("Email input was invalid")

        if type(bio) != str or len(bio) > 350:
            raise TypeError("Bio input was invalid")
        
        if type(avatar_url) != str or len(avatar_url) > 200:
            raise TypeError("avatar_url input was invalid")
            
        if id and type(id) != int:
            raise TypeError("ID input was invalid")
    
        self.name = name
        self.email = email
        self.bio = bio
        self.avatar_url = avatar_url
        self.id = id
    
        return


class ProfileEditForm(FlaskForm):

    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=100)])
    email = EmailField('Email', validators=[DataRequired()])
    bio = StringField("Bio", validators=[DataRequired(), Length(min=10, max=350)], widget=TextArea())
    avatar = URLField("Avatar URL")

