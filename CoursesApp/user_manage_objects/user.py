from flask_login import UserMixin
from flask_wtf import FlaskForm
from wtforms import BooleanField, EmailField, PasswordField, StringField
from wtforms.validators import InputRequired, Length
from .userprofile import Profile


class Role():

    def __init__(self, name, permission_level, id=None):
        if type(name) != str or len(name) > 10:
            raise TypeError("role name must be 10 characters or less")
        if type(permission_level) != int:
            raise TypeError("permission level must be an int")
        self.id = id
        self.name = name
        self.permission_level = permission_level


class User(UserMixin):
    
    def __init__(self, username, password, role=None, profile=None):
        
        if type(password) != str or len(password) > 102:
            raise TypeError("Password input was invalid")
        
        if type(username) != str or len(username) > 20:
            raise TypeError("Username input was invalid")
        
        if role and type(role) != Role:
            raise TypeError("role input was invalid")
        
        if profile and type(profile) != Profile:
            raise TypeError("profile input was invalid")
        
        self.username = username
        self.password = password

        if not role:
            role = Role('member', 1, 2)
        
        if not profile:
            profile = Profile.default()

        self.profile = profile
        self.role = role

    def get_id(self):
        return self.username


class SignupForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired(), Length(min=3, max=20)])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8)])


class LoginForm(SignupForm):
    remember_me = BooleanField('Remeber me')

class PasswordForm(SignupForm):
    username = None
    newpassword = PasswordField('New password', validators=[InputRequired(), Length(min=8)])