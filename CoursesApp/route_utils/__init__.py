from flask import redirect, url_for, session, flash, request
from flask_login import current_user, login_manager
from functools import wraps
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField


def redirect_next(fallback):
    """Redirects the user to the ?next parameter in the route.
        Useful when the user get's redirected to a page because they arent logged in.
    Args:
        fallback (str): The route that it should redirect to in the case that there is no next path

    Returns:
        redirect: the proper redirect
    """
    dest = request.args.get('next')
    
    if dest is None:
        return redirect(fallback)
    
    return redirect(dest)


def redirect_referrer(fallback):
    """Redirects the user back to wherever they came from.

    Args:
        fallback (str): The route that it should redirect to in the case that there is no referrer 

    Returns:
        redirect: the proper redirect
    """
    if request.referrer:
        return redirect(request.referrer)
    return redirect(url_for(fallback))


def requires_admin(func):
    """
    Makes a route only avalible to users with admin permissions.
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        if not current_user.is_authenticated:
            flash("You need to be logged in to access that page")
            return redirect(url_for('auth.login'))

        if current_user.role.permission_level < 2:
            flash("You need to be logged in with admin permissions to access that page")
            return redirect_referrer('auth.login')

        return func(*args, **kwargs)

    return decorator


def requires_permission_level(level=0):
    """Makes a route only avalible to users above or equal to the given permission level

    Args:
        level (int): The minimum permission level required to access the route
    """
    def requires_permission_level_inner(func):
        @wraps(func)
        def decorator(*args, **kwargs):
            if not current_user.is_authenticated:
                flash("You need to be logged in to access that page")
                return redirect(url_for('auth.login'))
            if current_user.role.permission_level < level:
                flash("You have insufficient permissions to access that page")
                return redirect(request.referrer)

            return func(*args, **kwargs)
        return decorator
        
    return requires_permission_level_inner
