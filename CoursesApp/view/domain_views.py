from flask import Blueprint, abort, escape, flash, redirect, render_template, request, url_for
import oracledb

from CoursesApp.dawson_db_objects.domain import Domain, DomainForm
from ..dbmanager import get_db
from flask_login import login_required
from ..route_utils import requires_permission_level


bp = Blueprint('domains', __name__, url_prefix='/domains')


@bp.route("/", methods=['GET', 'POST'])
def domain_list():
    domains = get_db().get_domains()
    return render_template('domains.html', Domain_List=domains)


@bp.route('/<domain_id>', methods=['GET', 'POST'])
def get_domain(domain_id):
    domain_id = escape(domain_id)
    spec_domain = get_db().get_domain(domain_id)
    if spec_domain is None:
        flash('ERROR! Invalid domain!')
        return redirect(url_for('domains.domain_list'))
    return render_template('domain.html', domain=spec_domain)


@bp.route('/add-domain', methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_domain():
    form = DomainForm()
    try:
        domains = get_db().get_domains()
        if request.method == 'POST' and form.validate_on_submit:
            domain = Domain(0, form.domain.data, form.description.data)
            # checking if domain added in form is already in db
            for stored_domain in domains:
                if stored_domain.domain == domain.domain and stored_domain.description == domain.description:
                    flash('ERROR! The domain already exists!')
                get_db().insert_domain(domain)
                return redirect(url_for('domains.domain_list'))
    except oracledb.Error as e:
        domains = None
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        domains = None
        flash(f"There was an error adding that domain: {e}")
    if not domains or len(domains) == 0:
        abort(404)
    return render_template('add_domain.html', form=form)


@bp.route("/<domain_id>/modify-domain", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def modify_domain(domain_id):
    db = get_db()
    try:
        old_domain = db.get_domain(domain_id)
        form = DomainForm(domain=old_domain.domain,
                          description=old_domain.description)
        domains = db.get_domains()

        if old_domain is None:
            flash('ERROR! Invalid domain!')
            return redirect(url_for('domains.domain_list'))

        if request.method == "POST" and form.validate_on_submit:
            update_domain = Domain(
                old_domain.id, form.domain.data, form.description.data)

            for sing_domain in domains:
                if sing_domain == update_domain:
                    flash("ERROR! The domain already exists")

            db.modify_domain(update_domain)
            return redirect(url_for('domains.get_domain', domain_id=update_domain.id))
    except oracledb.Error as e:
        flash(
            f"Unable to connect to the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error modifying that domain: {e}")
    return render_template('modify_domain.html', form=form, domain=old_domain)


@bp.route("/<domain_id>/delete-domain")
@login_required
@requires_permission_level(1)
def delete_domain(domain_id):
    try:
        get_db().delete_domain(domain_id)
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error deleting that domain: {e}")
    return redirect(url_for('domains.domain_list'))
