import oracledb
from oracledb.exceptions import IntegrityError
from flask import (Blueprint, abort, flash, redirect, render_template, request,
                   url_for)
from flask_login import login_required, login_user, logout_user, current_user
from werkzeug.security import check_password_hash, generate_password_hash
from ..route_utils import redirect_next
from ..dbmanager import get_db

from ..user_manage_objects.user import LoginForm, SignupForm, User, PasswordForm

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register/', methods=['GET', 'POST'])
def sign_up():
    """
    Registration route - Creates a user using the input user and password and logs in the user.
    """
    form = SignupForm()
    if request.method == 'POST':

        if form.validate_on_submit():

            pwd_hash = generate_password_hash(form.password.data)
            username = form.username.data

            try:

                user = User(username, pwd_hash)
                user = get_db().create_user(user)
                get_db().log_login(username)
                login_user(user)

                return redirect(url_for('profile.edit_profile'))

            except TypeError as e:
                flash(f'Username or password were invalid')
            except IntegrityError as e:
                flash(f'That user already exists')
            except oracledb.Error:
                flash("The user could not be created, contact webmaster or try again later")

    return render_template('register.html', form=form)


@bp.route('/login/', methods=['GET', 'POST'])
def login():
    """
    Login route - Authenticates user through username and password.
    """
    form = LoginForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            username = form.username.data
            user: User = get_db().get_user(username)
            if user:

                pwd = form.password.data
                if check_password_hash(user.password, pwd):
                    get_db().log_login(username)
                    login_user(user, remember=form.remember_me.data)
                        
                    return redirect_next(url_for('home.home'))
                else:
                    flash("Username or password was invalid!")
            else:
                flash("Username or password was invalid")

        else:
            flash("Form was invalid")

    return render_template('login.html', form=form)


@bp.route('/logout/')
@login_required
def logout():
    """
    Logs out the user
    """
    logout_user()
    return redirect(url_for('home.home'))


@bp.route('/editpassword', methods=['GET', 'POST'])
@login_required
def edit_password():
    
    form = PasswordForm()
    form.password.label = "Old password"
    
    if request.method == 'POST':
        
        if form.validate_on_submit():
            
            oldpassword = form.password.data
            newpassword = form.newpassword.data
            
            if check_password_hash(current_user.password, oldpassword):
                get_db().reset_password(current_user.username, newpassword)
                flash("Password successfully changed, please login again!")
                return redirect(url_for('auth.logout'))
            
            flash("Old password was wrong, please try again.")
                
    return render_template('edit_passwd.html', form=form)
