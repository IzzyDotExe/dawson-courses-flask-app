from flask import Blueprint, redirect, render_template, request, flash, url_for
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from oracledb import IntegrityError
import oracledb
from werkzeug.security import check_password_hash, generate_password_hash
from wtforms import StringField, SelectField
from ..route_utils import redirect_referrer, requires_admin
from ..dbmanager import get_db
from ..user_manage_objects.user import SignupForm, User

bp = Blueprint('admin', __name__, url_prefix='/admin/')


@bp.route('/users/', methods=['GET', 'POST'])
@login_required
@requires_admin
def admin_users():
    """
    admin_users route will display a list of all the users and all the options to modify them.
    """
    users = get_db().get_users()
    
    # Dynamically creates the form based on how many users there are
    class Form(FlaskForm):
        pass

    # Gets the roles that the user is allowed to give
    roles = [(x.id, x.name) for x in get_db().get_roles() if x.permission_level <= current_user.role.permission_level]

    # Create a select field for every user
    for user in users:

        choices = [(user.role.id, user.role.name)]

        if user.role.permission_level < current_user.role.permission_level and user.username != current_user.username:
            others = [x for x in roles if x[0] != user.role.id]

            for x in others:
                choices.append(x)

        # adds an attribute retroactivley to the form class
        setattr(Form, f'{user.username}-role', SelectField('role', choices=choices))

    form = Form()

    if request.method == 'POST':
        if form.validate_on_submit():
            datakeys = [x for x in form.data.keys() if x.endswith('role')]
            for key in datakeys:
                username = key.split('-')[0]
                user = get_db().get_user(username)
                user.role.id = int(form.data[key])
                get_db().update_user(user)
            flash('User groups were changed!')

    return render_template('admin_users.html', users=users, form=form)


@bp.route('/createuser', methods=['GET', 'POST'])
@login_required
@requires_admin
def create_user():
    """
    Used to create new users in the admin dashboard. Mostily similar to auth.signup but does not redirect to the profile and does not automatically login the user.
    """
    form = SignupForm()

    if request.method == 'POST':

        if form.validate_on_submit():

            pwd_hash = generate_password_hash(form.password.data)
            username = form.username.data

            try:

                user = User(username, pwd_hash)
                user = get_db().create_user(user)

                return redirect(url_for('admin.admin_users'))

            except TypeError as e:
                flash(f'Username or password were invalid')
            except IntegrityError as e:
                flash(f'That user already exists')
            except oracledb.Error:
                flash("The user could not be created, contact webmaster or try again later")

    users = get_db().get_users()
    return render_template('create_user.html', users=users, form=form)


@bp.route('/deleteuser/<string:username>')
@login_required
@requires_admin
def delete_user(username):
    """
    Used to delete a user witin the admin dashboard.
    """

    user = get_db().get_user(username)

    if user is None:
        flash("That user does not exist")
        return redirect_referrer('admin.admin_users')

    # all three of the following cases should block the delete.

    # Check if the user is trying to delete themselves
    if current_user == user:
        flash('You cannot delete yourself!')
        return redirect_referrer('admin.admin_users')

    # Check if the user is trying to delete a user who has a better role
    if current_user.role.permission_level < user.role.permission_level:
        flash('You cannot delete a user whos permissions are higher than yours!')
        return redirect_referrer('admin.admin_users')

    # Check if the user is trying to delete someone equal to them
    if current_user.role.permission_level == user.role.permission_level:
        flash('You cannot delete a user whos permissions are equal to yours!')
        return redirect_referrer('admin.admin_users')

    get_db().delete_user(user)

    return redirect_referrer('admin.admin_users')


@bp.route('/editpassword/<string:username>')
@login_required
@requires_admin
def reset_password(username):
    """
    Used to delete a user witin the admin dashboard.
    """

    user = get_db().get_user(username)
  

    if user is None:
        flash("That user does not exist")
        return redirect_referrer('admin.admin_users')
    # all three of the following cases should block the delete.
    # Check if the user is trying to delete themselves
    if current_user == user:
        flash('Please edit your own password using the user settings menu')
        return redirect_referrer('admin.admin_users')

    # Check if the user is trying to delete a user who has a better role
    if current_user.role.permission_level < user.role.permission_level:
        flash('You cannot reset a user whos permissions are higher than yours!')
        if request.referrer:
            return redirect(request.referrer)

        return redirect(url_for('admin.admin_users'))

    # Check if the user is trying to delete someone equal to them
    if current_user.role.permission_level == user.role.permission_level:
        flash('You cannot reset a user whos permissions are equal to yours!')
        return redirect_referrer('admin.admin_users')

    # PASSWORD RESET FUNCTION
    get_db().reset_password(user.username, "dawson1234")
    flash(f"{user.username}'s password is now set to 'dawson1234'")


    return redirect_referrer('admin.admin_users')

