from flask import Blueprint, abort, flash, redirect, render_template, request, escape, url_for
import oracledb
from ..route_utils import redirect_referrer
from flask_wtf import FlaskForm
from CoursesApp.dawson_db_objects.course import Course, CourseForm, CourseElement
from ..dbmanager import get_db
from flask_login import login_required
from ..route_utils import requires_permission_level


bp = Blueprint('courses', __name__, url_prefix='/courses/')


@bp.route('/')
def course_list():
    courses = get_db().get_courses()
    if courses == None:
        abort(404)
    return render_template('courses.html', Course_List=courses)


@bp.route('/<course_id>/')
def get_course(course_id):
    course_id = escape(course_id)
    spec_course = get_db().get_course(course_id)
    if spec_course is None:
        flash('ERROR! Invalid course!')
        return redirect(url_for('courses.course_list'))
    return render_template('course.html', course=spec_course)


@bp.route('/add-course/', methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_course():
    form = CourseForm()
    domains = get_db().get_domains()
    terms = get_db().get_terms()
    form.domain.choices = [(domain.id, domain.__repr__())
                           for domain in domains]
    form.term.choices = [(term.id, term.id) for term in terms]
    try:
        courses = get_db().get_courses()
        if request.method == 'POST' and form.validate_on_submit:
            course = Course(form.course_id.data, form.course_title.data, form.theory_hours.data,
                            form.lab_hours.data, form.work_hours.data, form.description.data, int(form.domain.data), int(form.term.data))
            # checking if course added in form is already in db
            for stored_course in courses:
                if stored_course.course_id == course.course_id:
                    flash('ERROR! The course already exists!')
                else:
                    get_db().insert_course(course)
                    return redirect(url_for('courses.course_list'))
                break
    except oracledb.Error as e:
        courses = None
        flash(f'Unable to connect with the database: {e}')
    except Exception as e:
        flash(f"Course creation failed: {e}")
        
    if not courses or len(courses) == 0:
        abort(404)
    return render_template('add_course.html', form=form)


@bp.route("/<course_id>/modify-course/", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def modify_course(course_id):
    db = get_db()
    try:
        old_course = db.get_course(course_id)
        form = CourseForm(course_id=old_course.course_id,
                          course_title=old_course.course_title,
                          theory_hours=old_course.theory_hours,
                          lab_hours=old_course.lab_hours,
                          work_hours=old_course.work_hours,
                          description=old_course.description,
                          domain=old_course.domain,
                          term=old_course.term)
        domains = get_db().get_domains()
        terms = get_db().get_terms()
        form.domain.choices = [(domain.id, domain.__repr__()) for domain in domains]
        form.term.choices = [(term.id, term.id) for term in terms]
        courses = db.get_courses()

        if old_course is None:
            flash('ERROR! Invalid course!')
            return redirect(url_for('courses.course_list'))

        if request.method == "POST" and form.validate_on_submit:
            update_course = Course(
                form.course_id.data, form.course_title.data, form.theory_hours.data,
                form.lab_hours.data, form.work_hours.data, form.description.data,
                int(form.domain.data), int(form.term.data))

            for sing_course in courses:
                if sing_course == update_course:
                    flash("ERROR! The course already exists")

            db.modify_course(update_course)
            return redirect(url_for('courses.get_course', course_id=update_course.course_id))
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error modifying that course: {e}")
        
    return render_template('modify_course.html', form=form)


@bp.route("/<course_id>/delete-course/")
@login_required
@requires_permission_level(1)
def delete_course(course_id):
    try:
        get_db().delete_course(course_id)
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error deleting that course: {e}")
    return redirect(url_for('courses.course_list'))


@bp.route('/<course_id>/elements/', methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_elements(course_id):

    course = get_db().get_course(course_id)

    if course is None:
        flash("That course does not exist!")
        return redirect_referrer('courses.course_list')

    form = CourseElement()
    elements = get_db().get_all_elements()
    form.element.choices = [(x.id, f"{x.id}: {x.element} - {x.criteria[:60]}...") for x in elements]

    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                get_db().add_course_element(course_id, int(form.element.data), form.hours.data)

            except ValueError as e:
                flash(str(e))

    course.total_hours = (course.lab_hours + course.theory_hours) * 15
    course.elements = get_db().get_course_elements(course.course_id)
    course.elementstotal = sum([x[1] for x in course.elements])
    return render_template('course_elements.html', form=form, course=course)


@bp.route('/<course_id>/elements/<int:element_id>/delete/')
@login_required
@requires_permission_level(1)
def remove_element(course_id, element_id):
    course = get_db().get_course(course_id)

    if course is None:
        flash("That course does not exist!")
        return redirect_referrer('courses.course_list')

    try:
        get_db().remove_course_element(course_id, element_id)
    except ValueError as e:
        flash(str(e))
    except oracledb.Error as e:
        print(e)
        flash("There was a database error, contact a webmaster")
    except Exception as e:
        flash("There was an unknown error, contact a webmaster")

    if request.referrer:
        return redirect_referrer('courses.course_list')


    return redirect(url_for('courses.add_elements', course_id=course_id))

