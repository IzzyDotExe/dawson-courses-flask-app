import oracledb
from flask import Blueprint, abort, flash, redirect, render_template, request, url_for, session
from flask_login import current_user, login_required
from ..user_manage_objects.userprofile import Profile, ProfileEditForm
from ..dbmanager import get_db
from ..route_utils import redirect_referrer, requires_admin, requires_permission_level

bp = Blueprint('profile', __name__, url_prefix='/profile')


@bp.route('/', methods=['GET', 'POST'])
@login_required
def my_profile():
    """
    Redirects to the current user's profile.
    """
    return redirect(url_for('profile.view_profile', username=current_user.username))


@bp.route('/<string:username>/')
def view_profile(username):
    """
    View a user's profile based on the username.
    """
    user = get_db().get_user(username)
    profile = user.profile
    return render_template('profile.html', profile=profile, user=user)


@bp.route('/all')
def view_all_profiles():
    """
    View all the current registered user's profiles.
    """
    
    users = get_db().get_users()

    # Make sure the current user is the first in the list.
    userlist = []

    if current_user.is_authenticated:
        userlist = [current_user]

    userlist.extend([x for x in users if x != current_user])
    users = userlist

    return render_template('profiles.html', users=users)


@bp.route('/edit/<string:username>/', methods=['GET', 'POST'])
@login_required
def edit_profile_user(username):
    """
    Edits a user profile based on their username.
    """

    user = get_db().get_user(username)
    profile = user.profile

    # If the edit target is not equal to the current user then we want to make sure
    # that the user has the sufficient permissions to edit this user.
    if current_user != user:

        # Checks if the current users permission level is lower than the target.
        if current_user.role.permission_level < user.role.permission_level:
            flash('You cannot edit the profile of a user whos permissions are higher than yours!')
            return redirect_referrer('profile.my_profile')

        # checks if the current user's permission level is equal
        if current_user.role.permission_level == user.role.permission_level:
            flash('You cannot edit the profile of a user whos permissions are equal to yours!')
            return redirect_referrer('profile.my_profile')

        # Here we aren't using <= because we want the flash message to be different.

    form = ProfileEditForm(name=profile.name, email=profile.email, bio=profile.bio, avatar=profile.avatar_url)

    if request.method == 'POST':

        if form.validate_on_submit():

            try:

                profile.name = form.name.data
                profile.bio = form.bio.data
                profile.email = form.email.data
                profile.avatar_url = form.avatar.data

                get_db().update_profile(profile)
                return redirect(url_for('profile.view_profile', username=username))

            except oracledb.Error as e:
                print(e)
                flash("Failed to update the profile")

    return render_template('profile_edit.html', profile=profile, form=form, user=user)


@bp.route('/edit/', methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def edit_profile():
    """
    Redirects to the edit page for the current user's profile.
    """
    return redirect(url_for('profile.edit_profile_user', username=current_user.username))

