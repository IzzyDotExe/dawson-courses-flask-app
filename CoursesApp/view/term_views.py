import oracledb.exceptions
from flask import Blueprint, abort, request, render_template, flash, redirect, url_for
from ..dawson_db_objects.term import Term, TermForm
from ..dbmanager import get_db

from flask_login import login_required
from ..route_utils import requires_permission_level

bp = Blueprint('terms', __name__, url_prefix='/terms')


@bp.route("/<int:term_id>")
def get_term(term_id):
    term = get_db().get_term(term_id)
    if term is None:
        flash("Error, id was invalid")
        return redirect(url_for('terms.term_list'))
    return render_template('term.html', term=term)


@bp.route('/', methods=['GET', 'POST'])
def term_list():
    terms = []
    try:
        terms = get_db().get_terms()
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error retrieving terms: {e}")
    return render_template('terms.html', terms=terms)


@bp.route('/add-term', methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_term():
    form = TermForm()
    try:
        terms = get_db().get_terms()
        if request.method == 'POST' and form.validate_on_submit:
            term = Term(0, form.year.data, form.season.data)
            # checking if term added in form is already in db
            for stored_term in terms:
                if stored_term.year == term.year and stored_term.name == term.name:
                    flash('ERROR! The term already exists!')
                get_db().insert_term(term)
                return redirect(url_for('terms.term_list'))
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error adding that term: {e}")
    if not terms or len(terms) == 0:
        abort(404)
    return render_template('add_term.html', form=form)


@bp.route("/<term_id>/modify-term", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def modify_term(term_id):
    db = get_db()
    try:
        old_term = db.get_term(term_id)
        form = TermForm(year=old_term.year, season=old_term.name)
        terms = db.get_terms()

        if old_term is None:
            flash('ERROR! Invalid term!')
            return redirect(url_for('term.term_list'))

        if request.method == "POST" and form.validate_on_submit:
            update_term = Term(old_term.id, form.year.data, form.season.data)

            for sing_term in terms:
                if sing_term == update_term:
                    flash("ERROR! The term already exists")

            db.modify_term(update_term)
            return redirect(url_for('terms.get_term', term_id=update_term.id))
    except oracledb.Error as e:
        flash(
            f"There was a problem in the Database! Try again later. Message: {str(e)}")
    except Exception as e:
        flash(f"There was an error modifying that term: {e}")
    return render_template('modify_term.html', form=form, term=old_term)


@bp.route("/<term_id>/delete-term")
@login_required
@requires_permission_level(1)
def delete_term(term_id):
    get_db().remove_term(term_id)
    return redirect(url_for('terms.term_list'))
