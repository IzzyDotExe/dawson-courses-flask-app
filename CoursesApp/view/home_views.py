from flask import Blueprint, render_template
from ..dbmanager import get_db
import humanize as h

bp = Blueprint('home', __name__, url_prefix='/')

@bp.route('/')
def home():
    users = get_db().get_recent_users()
    return render_template('home.html' ,users=users, hum=h)