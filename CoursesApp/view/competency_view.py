from flask import Blueprint, render_template, url_for, redirect, abort, flash, g, escape, request
from urllib.parse import urlparse
from oracledb import IntegrityError, Error

from CoursesApp.dawson_db_objects.element import Element, ElementForm
from ..dawson_db_objects.competency import Competency, CompetencyForm
from ..dbmanager import get_db
from flask_login import login_required
from ..route_utils import requires_permission_level

bp = Blueprint("competency", __name__, url_prefix="/competency")


@bp.route("/", methods=['GET', 'POST'])
def competency_list():
    competencies = []
    try:
        db = get_db()
        competencies = db.get_competencies()
    except Error as e:
        flash(
            f"ERROR! There was an Error in the server. Try again later! \n ({str(e)})")

    return render_template('competencies.html', Comp_List=competencies)


@bp.route("/<competency_id>/")
def get_competency(competency_id):

    db = get_db()
    competency_id = escape(competency_id)
    spec_comp = db.get_competency(competency_id)
    spec_comp_elements = db.get_elements(competency_id)

    if spec_comp is None:
        flash('ERROR! Invalid competency!')
        return redirect(url_for('competency.competency_list'))

    return render_template('specific_competency.html', found_comp=spec_comp, Comp_Element_List=spec_comp_elements)


@bp.route("/<competency_id>/elements/add-element/", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_elements(competency_id):
    form = ElementForm()
    try:
        elements = get_db().get_all_elements()
        comp_elements = get_db().get_elements(competency_id)
        if request.method == 'POST' and form.validate_on_submit:
            element = Element(get_db().get_next_id(), form.new_order.data, form.element.data,  # kept 0 here for the sake of python app, but will be overrided with db
                              form.criteria.data, competency_id)
            # checking if element added in form is already in db
            for stored_element in elements:
                if stored_element.id == element.id or stored_element == element:
                    flash('ERROR! The element already exists!')
                get_db().insert_element(competency_id, element)
                break
            return redirect(url_for(".view_elements", competency_id=competency_id))
    except IntegrityError as e:
        flash("ERROR! That element already exists!")
    except Error as e:
        flash(f'ERROR! There was a problem in the Server. Try again later')
    except Exception as e:
        elements = None
    if not elements or len(elements) == 0:
        abort(404)
    return render_template('add_element.html', form=form, element_list=comp_elements, comp_id=competency_id)


@bp.route("/<competency_id>/elements/")
def view_elements(competency_id):
    elements = get_db().get_elements(competency_id)
    elements.sort(key=lambda x: x.order)
    if elements is None:
        flash('ERROR! Invalid competency!')
        return redirect(url_for('competency.competency_list'))
    return render_template('elements.html', elements=elements, comp_id=competency_id)


@bp.route("/add-competency/", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def add_competency():
    try:
        form = CompetencyForm()
        db = get_db()
        competencies = db.get_competencies()

        if request.method == 'POST' and form.validate_on_submit:
            competency = Competency(
                form.competency_name.data, form.achievement.data, form.type.data, form.id.data)
            for singComp in competencies:
                if singComp.id == competency.id or singComp == competency:
                    flash("ERROR! The competency already exists!")

                db.add_compentency(competency)
                return redirect(url_for('.competency_list'))
    except IntegrityError as e:
        flash("ERROR! The competency already exist!")
    except Error as e:
        flash("ERROR! There was an error encountered in the server. Try again later or contact the Webmaster")
    except Exception as e:
        flash("ERROR! An error was encountered in this page. Try again later or Contact Webmaster")
        return redirect(url_for('.competency_list'))

    return render_template('add_competency.html', form=form, competency_list=competencies)


@bp.route("/<competency_id>/modify-competency/", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def modify_competency(competency_id):
    db = get_db()
    try:
        old_comp = db.get_competency(competency_id)
        form = CompetencyForm(competency_name=old_comp.competency,
                              achievement=old_comp.achievement, type=old_comp.type)
        competencies = db.get_competencies()

        if old_comp is None:
            flash('ERROR! Invalid competency!')
            return redirect(url_for('competency.competency_list'))

        if request.method == "POST" and form.validate_on_submit:
            update_comp = Competency(
                form.competency_name.data, form.achievement.data, form.type.data, old_comp.id)

            for sing_comp in competencies:
                if sing_comp.id == update_comp.id and sing_comp == update_comp:
                    flash("ERROR! The competency already exists")

            db.modify_competency(update_comp)
            return redirect(url_for('.get_competency', competency_id=update_comp.id))
    except IntegrityError as e:
        flash("ERROR! The competency was not updated! Try again!")
    except Error as e:
        flash("ERROR! A problem was encountered in the Server. Try again later or contact the Webmaster")
    except Exception as e:
        flash("ERROR! An error was encountered in this page. Try again later or Contact Webmaster")
    return render_template('modify_competency.html', form=form, competency=old_comp)


@bp.route("/<competency_id>/elements/<int:element_id>/element-modify/", methods=['GET', 'POST'])
@login_required
@requires_permission_level(1)
def modify_element(competency_id, element_id):
    db = get_db()
    elements = db.get_elements(competency_id)
    competencies = db.get_competencies()
    form = ElementForm()
    try:
        old_comp = db.get_competency(competency_id)
        old_elem = db.get_element(element_id)
        # TODO: May need to refactor code, but for now, the code works properly
        form = ElementForm(new_order=old_elem.order, element=old_elem.element,
                           criteria=old_elem.criteria, competency_id=old_elem.competency_id)
        form.competency_id.choices = [(comp.id, comp.id)
                                      for comp in competencies]

        if old_elem is None:
            flash("ERROR! Invalid element")
            return redirect(url_for('.view_elements', competency_id=old_comp.id))

        if request.method == "POST" and form.validate_on_submit:
            update_elem = Element(old_elem.id, form.new_order.data,
                                  form.element.data, form.criteria.data, form.competency_id.data)

            for sing_elem in elements:
                if sing_elem == update_elem:
                    flash("ERROR! The element already exists")

            db.update_element(update_elem)
            order_update(db, update_elem, elements, old_comp)
            return redirect(url_for(".view_elements", competency_id=old_comp.id))
    except IntegrityError as e:
        flash("ERROR! The element wasn't updated. Try again!")
    except Error as e:
        flash("ERROR! There was an error in the server. Contact Webmaster")
    except Exception as e:
        flash("ERROR! An error was encountered in this page. Try again later or Contact Webmaster")
    return render_template('modify_element.html', form=form, comp_id=old_comp.id)


@bp.route("/<competency_id>/delete-competency")
@login_required
@requires_permission_level(1)
def delete_competency(competency_id):
    try:
        db = get_db()
        competencies = db.get_competencies()
        db.delete_competency(competency_id)
        return redirect(url_for(".competency_list"))
    except IntegrityError as e:
        flash("ERROR! Can't delete the competency. Need to delete all the referenced elements first. ")
    except Error as e:
        flash("ERROR! An error was encountered in the Server. Try again later or contact the Webmaster")
    except Exception as e:
        flash("ERROR! An error was encountered in this page. Try again later or Contact Webmaster")
    return render_template('competencies.html', Comp_List=competencies)


@bp.route("/<competency_id>/elements/<int:element_id>/element-delete/")
@login_required
@requires_permission_level(1)
def delete_element(competency_id, element_id):
    try:
        db = get_db()
        elements = db.get_elements(competency_id)
        db.delete_element(element_id)
        return redirect(url_for(".view_elements", competency_id=competency_id))
    except Error as e:
        flash("ERROR! An error was encountered in the Server. Try again later or contact Webmaster.")
    except Exception as e:
        flash("ERROR! An error was encountered in this page. Try again later or Contact Webmaster")
    return redirect(url_for('.view_elements', competency_id=competency_id))


# TODO: Use Israel function to return to last page
@bp.route("/<competency_id>/<path:path>")
def catch_all(competency_id, path):
    url_part = urlparse(path)
    url_part = url_part.geturl()

    if url_part.__contains__('elements'):
        flash("ERROR! You can only modify or delete an element. You can't view a specific element alone")
        return redirect(url_for('.view_elements', competency_id=competency_id))

    flash("ERROR! This URL is wrong!")
    return redirect(url_for('.get_competency', competency_id=competency_id))

# this is a helper method. This is just so that, there will always be an order that is equal to 1.
# It is just so that it makes sense. There can't be an order of 2 of there is no order equal to 1.


def order_update(db, update_elem, old_elements, old_comp):
    # Checks if all the elements doesn't posseses an order equal to 1
    set_first_order = all([elem != 1 for elem in old_elements])

    if set_first_order == True or update_elem.competency_id == old_comp.id:
        order_update_elem = db.get_elements(update_elem.competency_id)
        order_update_elem.sort(key=lambda x: x.order)
        order_update_elem[0].order = 1
        db.update_element(order_update_elem[0])
