import secrets

from flask import Flask, render_template, get_flashed_messages, flash
from flask_login import LoginManager, current_user

from .dbmanager import close_db, get_db, init_db_command


def create_app(test_config=None):
    app = Flask(__name__)

    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32)
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    app.teardown_appcontext(cleanup)

    init_app(app)

    register_events(app, login_manager)

    return app


def init_app(app: Flask):

    app.cli.add_command(init_db_command)

    from .view.auth_views import bp as auth_bp
    app.register_blueprint(auth_bp)

    from .view.competency_view import bp as comp_bp
    app.register_blueprint(comp_bp)

    from .view.profile_views import bp as profile_bp
    app.register_blueprint(profile_bp)

    from .view.home_views import bp as home_bp
    app.register_blueprint(home_bp)

    from .view.admin_views import bp as admin_bp
    app.register_blueprint(admin_bp)

    from .view.domain_views import bp as dom_bp
    app.register_blueprint(dom_bp)

    from .view.term_views import bp as term_bp
    app.register_blueprint(term_bp)

    from .view.course_views import bp as course_bp
    app.register_blueprint(course_bp)


def register_events(app: Flask, login_manager: LoginManager):

    @login_manager.user_loader
    def load_user(user_id):
        return get_db().get_user(user_id)

    @app.errorhandler(404)
    def page_not_found(error):
        return "Not found!", 404

    @login_manager.user_loader
    def load_user(user_id):
        return get_db().get_user(user_id)

    @app.errorhandler(404)
    def page_not_found(error):
        return "Not found!", 404

    @app.before_request
    def before_request():
        blockedmsg = "You are blocked! You now have minimal permissions."
        if current_user.is_authenticated:
            if current_user.role.permission_level == 0:
                if blockedmsg not in get_flashed_messages():
                    flash(blockedmsg)


def cleanup(value):
    close_db()
