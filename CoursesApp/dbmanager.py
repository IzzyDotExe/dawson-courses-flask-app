import os

import click
from flask import current_app, g

from .db import Database


def get_db() -> Database:
    """ Returns the database object if it already exists and creates it if it doesnt.

    Returns:
        Database: database object
    """
    if 'db' not in g:
        g.db = Database()

    return g.db


def close_db():
    if 'db' in g:
        g.db.close()
        g.db = None


def init_db():
    db = get_db()
    db.run_file(os.path.join(current_app.root_path, 'sql/user_schema.sql'))


@click.command('init-db')
def init_db_command():
    init_db()
