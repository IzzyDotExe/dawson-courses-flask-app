
document.addEventListener('DOMContentLoaded', main)

let tableitems = [];

function main(event) {

    let flashCloseButton = document.querySelector("#flash .message-box > div > a")
    if (flashCloseButton) {
        flashCloseButton.addEventListener('click', closeFlashWindow)
    }

    let usertable = document.querySelector("table#search-table")
    let searchfield = document.querySelector('input#search-input')

    if (usertable && searchfield) {
        setupSearch(usertable, searchfield)
    }

}


function closeFlashWindow() {
    let flashwindow = document.querySelector("#flash")
    document.body.removeChild(flashwindow)
}

/**
 * 
 * @param {HTMLTableElement} usertable 
 * @param {HTMLInputElement} searchfield 
 */
function setupSearch(usertable, searchfield) {
    for (let i = 0; i < usertable.rows.length; i++) {
        const element = usertable.rows[i];
        if (element.id !== "title")
            tableitems.push(element)
    }
    searchfield.addEventListener('input', searchFunction)
    searchfield.table = usertable
}


function searchFunction(event) {

    if (!tableitems)
        return

    if (!event.target.value)
        buildTable(event.target.table, tableitems)

    if (event.target.value)
        buildTable(
            event.target.table, 
            
            tableitems.filter(
                x => 

                x.id.toLowerCase().includes(
                    event.target.value.toLowerCase()
                ) 

                || 

                x.innerText.toLowerCase().includes(
                    event.target.value.toLowerCase()
                )

            )
        )

}

/**
 * 
 * @param {HTMLTableElement} table 
 */
function buildTable(table, items) {
    
    console.log(items)

    for (let i = 0; i < table.rows.length; i++) {
        let element = table.rows[i];
        if (element.id !== "title")
            table.deleteRow(i)
    }

    items.forEach(element => {
        table.tBodies[0].appendChild(element)
    });


}