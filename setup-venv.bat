@echo off

echo Creating venv...
py -m venv .venv

echo Installing requirements...
.\.venv\Scripts\pip install -r requirements.txt

echo Venv created and deps installed! To activate run .\.venv\Scripts\activate